# rust-ldap-authorizer

A small ldap authorizer, made in Rust.
You can configure it with ldap server to authenticate to, an read-only user, pattern to match for, and then give it a username and password, so it can check if the query is in the remote ldap-server.