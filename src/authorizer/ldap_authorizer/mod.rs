use crate::authorizer::abstraction;

pub fn auth() {
    println!("Hello from authorizer!");
    println!("Now trying to access abstraction from authorizer");
    abstraction::abstra();
}