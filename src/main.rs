extern crate rust_external_authorization_manager;

fn main() {
    println!("Starting program!");
    rust_external_authorization_manager::authorizer::ldap_authorizer::auth();
}